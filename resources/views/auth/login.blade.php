@extends('layout')

@section('content')
    <h1>LOGIN</h1>
    <br>
    @if(session()->get('error'))
        <div style="background-color:red; color:white;">
            {{ session('error') }}
        </div>   
    @endif

    <form action="{{ route('login') }}" method="post">
    @csrf
        <div>
            <label for="email">Email</label>
            <input type="email" name="email">
            @error('email') {{ $message }} @enderror
        </div>
        <div>
            <label for="password">Password</label>
            <input type="password" name="password">
            @error('password') {{ $message }} @enderror
        </div>
        <button type="submit">LOGIN</button>
    </form>
@endsection