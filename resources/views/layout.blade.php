<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>
</head>
<body>
    <ul>
        <li><a href="#">HOME</a></li>
        @auth
        <li><a href="#">INI</a></li>
        <li>
            <form action="/logout" method="post">
            @csrf
            <button type="submit">Logout</button>
            </form>
        </li>
        <li><a href="#">{{ auth()->user()->name }}</a></li>
        @endauth

        @guest
        <li><a href="#">TAMU</a></li>
        @endguest
    </ul>
    

    @yield('content')
</body>
</html>