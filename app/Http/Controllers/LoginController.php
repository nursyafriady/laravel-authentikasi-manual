<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class LoginController extends Controller
{
    public function formLogin()
    {
        return view ('auth.login');
    }

    public function login()
    {
        $attr = request()->validate([
            'email' => 'required',
            'password' => 'required'
        ]);

        $user = User::whereEmail(request('email'))->first();
        if ($user) {
            Auth::login($user);
            return redirect()->intended('/');
        } else {
            return back()->with('error', 'email tidak ditemukan');
        }
        // dd($user);

        // if (Auth::attempt($attr)) {
        //     // Authentication passed...
        //     return redirect()->intended('/');
        // }
    }
}
